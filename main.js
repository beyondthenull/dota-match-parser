var defaults = require('superagent-defaults')
var Throttle = require('superagent-throttle');

var throttle = new Throttle({
  rate: 1,
  ratePer: 1000,
  concurrent: 1
});

var request = defaults();
request.use(throttle.plugin);

var steamKey = process.env.STEAMKEY;
var matchID = process.argv[2];

if(!matchID) {
  console.log('Must specify match ID!');
  process.exit(-1)
}

var apiURL = 'http://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/v1';

request
  .get(apiURL)
	.query({ key: steamKey })
  .query({ match_id: matchID })
  .end(function(err, res){
    var matchResult = res.body.result;

    if(matchResult.error) {
      console.log('Error: ' + matchResult.error);
      return;
    }

    var winningTeam = matchResult.radiant_win ? 'Radiant' : 'Dire';

  	console.log('Winner: ' + winningTeam);
    console.log('Radiant Kills: ' + matchResult.radiant_score);
    console.log('Dire Kills: ' + matchResult.dire_score);

    var durMin = Math.floor(matchResult.duration / 60);
    var durSec = matchResult.duration % 60;
    console.log('Duration: ' + durMin + ':' + durSec);

    var startTime = new Date(matchResult.start_time * 1000);
    console.log('Start Time: ' + startTime.toLocaleString());
  });
